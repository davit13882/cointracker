## CoinTracker - Android App

---

CoinTracker is a cryptocurrency prices application. 
Unlike other services, CoinTracker is free and open source.
You need to register on CoinTracker to see top 7 coins prices and detailed information

*CoinTracker is available on bitbucket as open source application

---

## About

CoinTracker has made for you to comfortably check and compare cryptocurrency prices. 

This android application allows you to login/regiser check cryptocurrency prices and also see further information about selected currency

This application was originally created by Davit Osadze and released under the GNU GPLv3.

---

## Features
---
The android app lets you:

1. Login and register on portal
2. Check different cryptocurrency prices
3. Open and check further information
4. Switch between dark and light themes
5. See cryptocurrency images

---

## Permissions
---
CoinTracker requires only network access permission, don't need any read and write access to external storage.

---

## Contributing

---

CoinTracker app is a free and open source project developed for volunteers by volunteer. Any contributions are welcome.

---

## License

---

This application is released under GNU GPLv3 (see LICENSE). Some of the used libraries are released under different licenses.