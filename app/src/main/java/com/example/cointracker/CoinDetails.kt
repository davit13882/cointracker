package com.example.cointracker

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_coin_details.*
import kotlinx.android.synthetic.main.coin_view.view.*

class CoinDetails : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_coin_details)
        init()

        backButton.setOnClickListener {
            super.onBackPressed()

        }

    }
    private fun init() {
        val coin_name:String = intent.getStringExtra("coin_name").toString()
        val coin_imageGet:String = intent.getStringExtra("coin_image").toString()
        val coin_priceGet:String = intent.getStringExtra("coin_price").toString()
        val about_coinGet:String = intent.getStringExtra("about_coin").toString()


        val lowGet:String = intent.getStringExtra("low1d").toString()
        val highGet:String = intent.getStringExtra("high1d").toString()
        val openGet:String = intent.getStringExtra("open1d").toString()

        head_title.text = coin_name
        Picasso.get().load(coin_imageGet).into(coin_image)
        coin_name_view.text = "$coin_name - $$coin_priceGet"

        lowview.text = "$$lowGet"
        highview.text = "$$highGet"
        openview.text = "$$openGet"
        about_coin.text = "$about_coinGet"





    }
}