package com.example.cointracker

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.OrientationHelper
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.example.cointracker.model.Data
import com.example.cointracker.model.Reqres
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val dataList: MutableList<Data> = mutableListOf()
    private lateinit var coinAdapter : CoinAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loadCoins()

        refreshButton.setOnClickListener {
            dataList.clear()
            loadCoins()
        }

    }

    private fun loadCoins() {
        coinAdapter = CoinAdapter(dataList)

        coin_recycler_view.layoutManager = LinearLayoutManager(this)
        coin_recycler_view.addItemDecoration(DividerItemDecoration(this,OrientationHelper.VERTICAL))
        coin_recycler_view.adapter = coinAdapter


        AndroidNetworking.initialize(this)
        AndroidNetworking.get("http://myguns.ge/cointracker/")
            .build()
            .getAsObject(Reqres::class.java, object : ParsedRequestListener<Reqres>{
                override fun onResponse(response: Reqres) {
                    dataList.addAll(response.data)
                    coinAdapter.notifyDataSetChanged()
                    Toast.makeText(this@MainActivity,"Loaded Succesfully", Toast.LENGTH_SHORT).show()
                }

                override fun onError(anError: ANError?) {
                    Toast.makeText(this@MainActivity,anError.toString(), Toast.LENGTH_SHORT).show()
                    Log.d("OvError", anError.toString())

                }

            })
    }
}