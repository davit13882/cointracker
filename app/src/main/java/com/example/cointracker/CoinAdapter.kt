package com.example.cointracker

import android.content.Context
import android.content.Intent
import android.util.Log
import android.util.Log.d
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.cointracker.model.Data
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.coin_view.view.*

class CoinAdapter(private val dataList: MutableList<Data>) : RecyclerView.Adapter<CoinHolder>() {
    private lateinit var context: Context
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): CoinHolder {
        context = parent.context
        return CoinHolder(LayoutInflater.from(context).inflate(R.layout.coin_view, parent, false))
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: CoinHolder, position: Int) {
        val data = dataList[position]

        val nameTextView = holder.itemView.coinName
        val symbolTextView = holder.itemView.coinSymbol
        val priceusdTextView = holder.itemView.priceUsd
        val low24hView = holder.itemView.low24h
        val high24hView = holder.itemView.high24h
        val open24hView = holder.itemView.open24h

        val name = "${data.name}"
        val symbol = "${data.symbol}"
        val priceusd = "${data.priceUsd}"
        val low = "${data.low24H}"
        val high = "${data.high24H}"
        val open = "${data.open24H}"

//        nameTextView.text = name
        symbolTextView.text = symbol
        priceusdTextView.text = priceusd
        low24hView.text = "$" + low
        high24hView.text = "$" + high
        open24hView.text = "$" + open

        Picasso.get().load(data.coin_image).into(holder.itemView.coinIcon)
        Log.d("coin_name", name)


        holder.itemView.setOnClickListener {
            val intent = Intent(context, CoinDetails::class.java)
            intent.putExtra("coin_name", name)
            intent.putExtra("coin_image", data.coin_image)
            intent.putExtra("about_coin", data.about_coin)
            intent.putExtra("coin_price", priceusd)
            intent.putExtra("low1d", low)
            intent.putExtra("high1d", high)
            intent.putExtra("open1d", open)

            context.startActivity(intent)
        }



    }
}

