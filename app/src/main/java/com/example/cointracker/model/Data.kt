package com.example.cointracker.model


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("id")
    val id: String,
    @SerializedName("coin_image")
    val coin_image: String,
    @SerializedName("about_coin")
    val about_coin: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("24_hours_low")
    val low24H: String,
    @SerializedName("24_hours_high")
    val high24H: String,
    @SerializedName("24_hours_open")
    val open24H: String,
    @SerializedName("price_btc")
    val priceBtc: String,
    @SerializedName("price_usd")
    val priceUsd: String,
    @SerializedName("symbol")
    val symbol: String
)